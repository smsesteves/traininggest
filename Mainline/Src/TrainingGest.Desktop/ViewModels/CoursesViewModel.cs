﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TrainingGest.Business;
using TrainingGest.Domain;


namespace TrainingGest.Desktop.ViewModels
{
    public class CoursesViewModel : Screen, IView
    {
        readonly ICoursesManager _coursesManager;

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => this.Name);
            }
        }

        public CoursesViewModel(ICoursesManager coursesManager)
        {
            _coursesManager = coursesManager;
        }
        public void Teste()
        {
            MessageBox.Show("Courses");
        }

        public void AddCourse()
        {
            try
            {
                var course = new Course { Name = _name, CreatedOn = DateTime.Now.AddYears(3) };
                _coursesManager.Add(course);
                MessageBox.Show("Adicionado com Sucesso");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
