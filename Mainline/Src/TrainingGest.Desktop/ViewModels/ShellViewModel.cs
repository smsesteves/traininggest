﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrainingGest.Business;

namespace TrainingGest.Desktop.ViewModels
{
    public class ShellViewModel : Conductor<IView>.Collection.OneActive, IShell
    {
        readonly IEventAggregator _eventAggregator;
        readonly IWindowManager _windowManager;

        private string _name;
        public string Name
        {
            get { return _name; }
            set 
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public DashboardViewModel DashboardViewModel { get; private set; }
        public CoursesViewModel CoursesViewModel { get; private set; }

        public ShellViewModel(IEventAggregator eventAggregator, IWindowManager windowManager,
            ICoursesManager coursesmanager, CoursesViewModel coursesViewModel, DashboardViewModel dashboardViewModel)
        {
            this.DisplayName = "Training Gest";
            this.Name = "TrainingGest";

            _eventAggregator = eventAggregator;
            _windowManager = windowManager;

            CoursesViewModel = coursesViewModel;
            DashboardViewModel = dashboardViewModel;

            //SIMPLE CONTAINER
            //Items.Add(this.CoursesViewModel);
            //Items.Add(this.DashboardViewModel);

            ActivateItem(this.DashboardViewModel);
        }

        public void ShowCourses()
        {
            ActivateItem(this.CoursesViewModel);
        }

        public void ShowDashboard()
        {
            ActivateItem(this.DashboardViewModel);
        }
    }
}
