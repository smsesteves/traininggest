﻿using Autofac;
using IContainer = Autofac.IContainer;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using TrainingGest.Business;
using TrainingGest.Desktop.ViewModels;

namespace TrainingGest.Desktop
{
    public class Bootstrapper<T> : BootstrapperBase
    {
        //SimpleContainer _container;

        protected IContainer Container { get; private set; }
        public Type ViewModelBaseType { get; set; }

        public Func<IWindowManager> CreateWindowManager { get; set; }
        public Func<IEventAggregator> CreateEventAggregator { get; set; }

        public Bootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            ConfigureBootstrapperInternal();

            var builder = new ContainerBuilder();
            builder.RegisterType<T>().As<IShell>().InstancePerDependency();
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray()).Where(type => type.Name.EndsWith("ViewModel"))
                                                                            .Where(type => !(string.IsNullOrWhiteSpace(type.Namespace)) && type.Namespace.EndsWith("ViewModels"))
                                                                            .Where(type => type.GetInterface(typeof(INotifyPropertyChangedEx).Name) != null)
                                                                            .AsSelf().InstancePerDependency();

            
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray()).Where(type => type.Name.EndsWith("View"))
                                                                            .Where(type => !(string.IsNullOrWhiteSpace(type.Namespace)) && type.Namespace.EndsWith("Views"))
                                                                            .AsSelf();

            builder.Register<IWindowManager>(c => CreateWindowManager()).InstancePerLifetimeScope();
            builder.Register<IEventAggregator>(c => CreateEventAggregator()).InstancePerLifetimeScope();

            ConfigureContainer(builder);
            Container = builder.Build();
        }


        protected override object GetInstance(Type service, string key)
        {
            if(string.IsNullOrWhiteSpace(key))
            {
                object obj;
                if(Container.TryResolve(service, out obj))
                {
                    return obj;
                }
            }
            else
            {
                object obj;
                if(Container.TryResolveNamed(key, service, out obj))
                {
                    return obj;
                }
            }

            throw new Exception("Could not locate any instances");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return Container.Resolve(typeof(IEnumerable<>).MakeGenericType(new [] { service })) as IEnumerable<object>;
        }

        protected override void BuildUp(object instance)
        {
            Container.InjectProperties(instance);
        }

        protected virtual void ConfigureContainer(ContainerBuilder builder)
        {

        }

        protected void ConfigureBootstrapperInternal()
        {
            ViewModelBaseType = typeof(INotifyPropertyChanged);
            CreateWindowManager = () => (IWindowManager)new WindowManager();
            CreateEventAggregator = () => (IEventAggregator)new EventAggregator();

            ConfigureBootstrapper();
        }

        protected virtual void ConfigureBootstrapper()
        {
            
        }

        //SIMPLE CONTAINER
        //protected override void Configure()
        //{
        //    _container = new SimpleContainer();

        //    _container.Singleton<IWindowManager, WindowManager>();
        //    _container.Singleton<IEventAggregator, EventAggregator>();
        //    _container.PerRequest<IShell, ShellViewModel>();
        //    _container.PerRequest<ICoursesManager, CoursesManager>();

        //    _container.PerRequest<IView, CoursesViewModel>("courses");
        //    _container.PerRequest<IView, DashboardViewModel>("dashboard");
            
        //}

        //protected override object GetInstance(Type service, string key)
        //{
        //    var instance = _container.GetInstance(service, key);
        //    if(instance != null)
        //    {
        //        return instance;
        //    }

        //    throw new Exception("Could not locate any instances");
        //}

        //protected override IEnumerable<object> GetAllInstances(Type service)
        //{
        //    return _container.GetAllInstances(service);
        //}

        //protected override void BuildUp(object instance)
        //{
        //    _container.BuildUp(instance);
        //}

        protected override void OnStartup(object sender, System.Windows.StartupEventArgs e)
        {
            DisplayRootViewFor<IShell>();   
        }
    }
}
