﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingGest.Business;
using TrainingGest.Desktop.ViewModels;
using Autofac;
using System.Data.Entity;
using TrainingGest.Data;
namespace TrainingGest.Desktop
{
    public class TrainingGestBootstrapper : Bootstrapper<ShellViewModel>
    {
        protected override void ConfigureBootstrapper()
        {
            ViewModelBaseType = typeof(IView);
            Database.SetInitializer<TrainingGestDbContext>(null);
                
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<CoursesManager>().As<ICoursesManager>().InstancePerDependency();
        }
    }
}
