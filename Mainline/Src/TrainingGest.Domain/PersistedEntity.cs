﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkAttitude.Kernel;

namespace TrainingGest.Domain
{
    public class PersistedEntity : IPersistedEntity
    {
        public long Id { get; set; }
    }   
}
