﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingGest.Domain
{
    public class Course : PersistedEntity
    {
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
