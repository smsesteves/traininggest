﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrainingGest.Web.Models;

namespace TrainingGest.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            CourseViewModel viewModel = new CourseViewModel { Name = "Pascal" };
            return View(viewModel);
        }

    }
}
