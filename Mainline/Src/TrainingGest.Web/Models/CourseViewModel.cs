﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TrainingGest.Web.Models
{
    public class CourseViewModel
    {
        [DisplayName("Name")]
        public string Name { get; set; }
    }
}