﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingGest.Domain;

namespace TrainingGest.Data
{
    public class TrainingGestContextInitializer : DropCreateDatabaseIfModelChanges<TrainingGestDbContext>
    {
        protected override void Seed(TrainingGestDbContext context)
        {
            context.Courses.Add(new Course { Name = "C#", CreatedOn = DateTime.Now });
            base.Seed(context);
        }
    }
}
