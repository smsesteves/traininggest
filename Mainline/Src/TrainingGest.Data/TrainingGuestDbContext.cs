﻿using System.Data.Entity;
using System.Data.Common;
using TrainingGest.Domain;
using TrainingGest.Data.Mappings;


namespace TrainingGest.Data
{
    public class TrainingGestDbContext : DbContext
    {
        public TrainingGestDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {

        }
        public TrainingGestDbContext() : base ("name=TrainingGestDbContext")
        {
            
        }

        //public TrainingGestDbContext(DbConnecion existingConnection, bool contextOwnsConnection)
        //    : base(existingConnection, contextOwnsConnection)
        //{

        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CourseMapping());
            
        }

        public DbSet<Course> Courses { get; set; }

    }
}
