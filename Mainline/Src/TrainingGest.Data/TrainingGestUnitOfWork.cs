﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkAttitude.Data;
using ThinkAttitude.Data.EntityFramework;
using TrainingGest.Domain;

namespace TrainingGest.Data
{
    public class TrainingGestUnitOfWork : UnitOfWork
    {
        
        public TrainingGestUnitOfWork()
        {
            Context = new TrainingGestDbContext();
            ObjectContext = ((IObjectContextAdapter)Context).ObjectContext;
        }

        private IRepository<Course> _coursesRepository;
        public IRepository<Course> CoursesRepository
        {
            get
            {
                return _coursesRepository ?? (_coursesRepository = new RepositoryBase<Course>(ObjectContext));
            }
        }
    }
}
