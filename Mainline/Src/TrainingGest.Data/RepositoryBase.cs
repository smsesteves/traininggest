﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkAttitude.Data;
using ThinkAttitude.Data.EntityFramework;
using ThinkAttitude.Kernel;

namespace TrainingGest.Data
{
    public class RepositoryBase<T> : Repository<T> where T : class
    {
        public RepositoryBase(ObjectContext context) : base(context)
        {

        }
    }
}
