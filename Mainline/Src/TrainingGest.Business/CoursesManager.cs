﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingGest.Data;
using TrainingGest.Domain;

namespace TrainingGest.Business
{
    public class CoursesManager : ICoursesManager
    {

        public void Add(Domain.Course course)
        {
            using (var unitOfWork = new TrainingGestUnitOfWork())
            {
                unitOfWork.CoursesRepository.Add(course);
                unitOfWork.Commit();
            }
        }
    }
}
