﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingGest.Console
{
    public class TrainingGestContextInitializer : DropCreateDatabaseIfModelChanges<TrainingGestDbContext>
    {
        protected override void Seed(TrainingGestDbContext context)
        {
            context.Courses.Add(new Course { Name = "C#" });
            base.Seed(context);
        }
    }
}
