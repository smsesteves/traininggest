﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingGest.Console
{
    public class UnitOfWork : IUnitOfWork
    {
        protected DbContext Context {get; private set;}

        protected ObjectContext ObjectContext { get; private set; }

        public UnitOfWork()
        {
            Context = new TrainingGestDbContext();
            ObjectContext = ((IObjectContextAdapter)Context).ObjectContext;
        }

        public int Commit()
        {
            return Context.SaveChanges();
        }

        public void Dispose()
        {
            if(Context != null)
            {
                Context.Dispose();
                Context = null;
            }

            GC.SuppressFinalize(this);
        }

        private IRepository<Course> _coursesRepository;
        public IRepository<Course> CoursesRepository
        {
            get
            {
                return _coursesRepository ?? (_coursesRepository = new Repository<Course>(ObjectContext));
            }
        }
    }
}
