﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingGest.Console
{
    public class UnitOfWork : IUnitOfWork
    {
        protected DbContext Context {get; private set;}

        public UnitOfWork()
        {
            Context = new TrainingGestDbContext();
        }

        public int Commit()
        {
            return Context.SaveChanges();
        }

        public void Dispose()
        {
            if(Context != null)
            {
                Context.Dispose();
                Context = null;
            }

            GC.SuppressFinalize(this);
        }
    }
}
