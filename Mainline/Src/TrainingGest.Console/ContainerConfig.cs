﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingGest.Business;

namespace TrainingGest.Console
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Application>().As<IApplication>().SingleInstance();

            builder.RegisterType<CoursesManager>().As<ICoursesManager>().InstancePerDependency();

            return builder.Build();
        }
    }
}
