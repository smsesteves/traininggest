﻿

using System.Data.Entity;
using System.Data.Common;

namespace TrainingGest.Console
{
    public class TrainingGestDbContext : DbContext
    {
        public TrainingGestDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {

        }
        public TrainingGestDbContext() : base ("name=TrainingGestDbContext")
        {
            
        }

        //public TrainingGestDbContext(DbConnecion existingConnection, bool contextOwnsConnection)
        //    : base(existingConnection, contextOwnsConnection)
        //{

        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CourseMapping());
            
        }

        public DbSet<Course> Courses { get; set; }

    }
}
