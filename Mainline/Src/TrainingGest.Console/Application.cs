﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using System.Data.Entity;
using TrainingGest.Data;
using TrainingGest.Domain;
using TrainingGest.Business;

namespace TrainingGest.Console
{
    public class Application : IApplication
    {
        private readonly ICoursesManager _coursesManager;
        public ILifetimeScope Resolver { get; private set; }

        public Application(ILifetimeScope resolver, ICoursesManager coursesManager)
        {
            Resolver = resolver;
            _coursesManager = coursesManager;
        }


        public void Run()
        {
            Database.SetInitializer<TrainingGestDbContext>(null);
            var course = new Course { Name = "C#", CreatedOn = DateTime.Now.AddYears(1) };
            _coursesManager.Add(course);
        }
    }
}
