﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThinkAttitude.Kernel;

namespace ThinkAttitude.Data.EntityFramework
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
       
        protected ObjectContext Context { get; private set; }

        protected IObjectSet<T> ObjectSet { get; private set; }

        public Repository(ObjectContext context)
        {
            Context = context;
            ObjectSet = Context.CreateObjectSet<T>();
        }

        public IQueryable<T> GetQuery()
        {
            return ObjectSet;
        }

        public IEnumerable<T> GetAll()
        {
            return GetQuery().ToList();
        }

        public IEnumerable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return ObjectSet.Where<T>(where);
        }

        public T Single(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return ObjectSet.SingleOrDefault<T>(where);
        }

        public T First(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return ObjectSet.First<T>(where);
        }

        public void Delete(T entity)
        {
            ObjectSet.DeleteObject(entity);
        }                                                   

        public void Add(T entity)
        {
            ObjectSet.AddObject(entity);
        }

        public void Attach(T entity)
        {
            ObjectSet.Attach(entity);
        }
    }
}
