﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinkAttitude.Data.EntityFramework
{
    public abstract class UnitOfWork : IUnitOfWork
    {
        protected DbContext Context {get; set;}

        protected ObjectContext ObjectContext { get; set; }

        public UnitOfWork() 
        {
        
        }

        public int Commit()
        {
            return Context.SaveChanges();
        }

        public void Dispose()
        {
            if(Context != null)
            {
                Context.Dispose();
                Context = null;
            }

            GC.SuppressFinalize(this);
        }

    }
}
